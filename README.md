# Supsig Specification #

<http://git.distrust.co/supsig>

## About ##

This document defines the specification for "supsig", a portable software
supply chain signing solution for code, code reviews, and reproducible artifact
builds.

## Status ##

This is currently a **DRAFT**

Current draft is in use at multiple organizations, but current reference
tooling such as "git-sig" uses version "v0" and may break at any time until we
stabilize at "v1".

Contributions at this stage are very welcome.

Please collaborate via Matrix at [https://matrix.to/#/#supsig:matrix.org]

## Definitions

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this
document are to be interpreted as described in RFC2119 when, and only when,
they appear in all capitals, as shown here.

## Goals

- Define a broadly applicable signing format for code, reviews, and artifacts
- Define system for anonymous submission and discovery of signatures

## Requirements

- MUST defend against a compromise of any single human or system
  - MUST exclusively rely on FOSS dependencies with signed reproducible builds
- MUST be portable with code archives or any known VCS system
- MUST support signing via the CLI, GUI, or web interfaces
  - Web support implies supporting native signing like Passkeys/Webauthn
- SHOULD support any cryptographic tooling supporting detached signatures
  - We SHOULD NOT attempt to force the FOSS community to migrate off of PGP
  - We SHOULD NOT attempt to force PGP on organizations with other solutions
- MUST support a minimal, yet optional, plaintext review format
- MUST support signatures that only indicate reproducible builds
- MUST allow signing a review from a wide range of alternative review systems
  - MAY be small plain-text snippets, or a hash of an external system
- MUST support all supply chain artifacts including reviews, code, and binaries
- MUST communicate and collaborate on project exclusively with FOSS tools
  - We want to be inclusive to the open source community, not just corporations
  - Examples: Forgejo/gitea issues, e-mail, IRC, matrix, jitsi, etc.
- SHOULD seek compatibility with popular supply chain security tooling
  - Examples: in-toto, witness, OCI container signing

## Format

```
"sig:$version:$vcs_ref:$tree_hash:$review_hash:$sig_type:$signature"
```

- version: (required) version of the supsig format
- vcs: (optional) vcs system used (ex, git, hg,svn,cvs,perforce,etc )
- tree_hash: (required) sha256 hash of the object or directory being signed
- review_hash: (optional) sha256 hash of a review from any review system
- sig_type: (required) signature type (webauthn, pgp, etc)
- signature: (required) actual ascii armored signature payload

## Examples

### Signature Generation

#### Shell: Git and GPG with plaintext review

```
review="LGTM"
review_hash="$(printf $review | openssl sha256 | awk '{print $2}')"
vcs_ref="$(git rev-parse HEAD)"
tree_hash="$(git rev-parse 'HEAD^{tree}')"
body="sig:v0:$vcs_ref:$tree_hash:$review_hash:pgp"
sig=$(printf "%s" "$body" | gpg --detach-sign  | openssl base64 -A )
printf "%s" "$body:$review:$sig"
```

### Signature Verification

#### PGP

```
gpg --verify <(printf "$sig_body") <(printf "$sig")
```

### Review Verification

TODO


## Background

TODO

### Motivation

TODO

### Alternatives

Here we will comment on other popular approaches to supply chain integrity,
and our opinion on what they get right, and where they fall short.

#### No signing

Many software distribution systems are very intentional about not supporting
signing at all. Some have expressed fears that some developers are intimidated
by the idea of signing code or reviews and would stop contributing entirely
such practices were normalized.

Attempts to enforce signing are often rejected as an attempt at gatekeeping,
Some distribution system maintainers argue that not signing at all is an
appropriate security posture given other mitigations such as the availability
of two factor authentication.

Years of supply chain attacks on systems with this posture have seemingly done
little to nothing to dissuade these positions.

##### npm

TODO

##### pip

TODO

##### brew

TODO

### Centralized signing

- sigstore/fulcio
- nix
- apk
- fulcio
- gitsign

### Distributed signing

- bitcoin: https://bitcoinops.org/en/topics/reproducible-builds/
- apt: https://wiki.debian.org/SecureApt
- pacman: https://wiki.archlinux.org/title/Pacman/Package_signing
- crev: ??
- git-wotr
- git-signatures
- sigstore
- cosign
- notary

## References ##

### Noteworthy Supply Chain Attacks ###

## Funding ##

This project is housed under the umbrella of Distrust, and self funded to meet
the growing needs of Distrust clients, and by extension the wider open source
community whose dependencies they heavily rely on.

Funding for this project and team in the current direction, or partnership with
existing projects with compatible goals would be very welcome.
